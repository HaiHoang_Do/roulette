import java.util.Scanner;

public class Roulette
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        int userMoney = 1000;
        
        boolean continueGame = true;
        
        while(continueGame)
        {
            if(userMoney > 0)
            {
                System.out.println("Would you like to play this round?");
                String answer = scan.next();
                if(answer.equals("no"))
                {
                    continueGame = false;
                }
                else if(answer.equals("yes"))
                {
                    int betAmount = moneySetup(scan, userMoney);
                    int betNumber = betSetup(scan);
                    boolean winLoss = betResult(wheel, betNumber);
                    if(winLoss)
                    {
                        userMoney += (betAmount*35);
                    }
                    else if(!winLoss)
                    {
                        userMoney -= betAmount;   
                    }
                    System.out.println("Your balance as of now is: " + userMoney);
                }
            }
            else
            {
                continueGame = false;
            }
        }
        System.out.println("The game is now over");
    }

    public static int moneySetup(Scanner scan, int userMoney)
    {
        int betAmount = 0;
        System.out.println("How much would you like to bet?");

        boolean validAmount = false;
        while(!validAmount)
        {
            betAmount = scan.nextInt();
            if(betAmount > userMoney)
            {
                System.out.println("Please select an amount smaller or equal to your current balance");
            }
            else
            {
                validAmount = true;
            }
        }
        return betAmount;
    }

    public static int betSetup(Scanner scan)
    {
        boolean validBet = false;

        System.out.println("What number would you like to bet on?");
        int betNumber = 0;
        
        while (!validBet)
        {
            betNumber = scan.nextInt();
            if(betNumber > 36 || betNumber < 0)
            {
                System.out.println("Please select a number ranging from 0 to 36");
            }
            else
            {
                validBet = true;
            }
        }
        return betNumber;
    }

    public static boolean betResult(RouletteWheel wheel, int betNumber)
    {
        wheel.spin();
        System.out.println("The result of this spin is: " + wheel.getValue());
        if(wheel.getValue() == betNumber)
        {
            System.out.println("You won this round!");
            return true;
        }
        else
        {
            System.out.println("You lost this round!");
            return false;
        }
    }
}